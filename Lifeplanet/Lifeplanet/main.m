//
//  main.m
//  Lifeplanet
//
//  Created by KIM Seongho on 2015. 7. 9..
//  Copyright (c) 2015년 com.Zikto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
